from django.contrib.auth.models import User
from django.urls import resolve, reverse
from django.test import TestCase
from .views import login

# Create your tests here.
class Story_9_Unit_Test(TestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.user = User.objects.create_user('jilham', 'jilham.luthfi15@gmail.com', 'test123')
        cls.user.first_name = 'jilham'
        cls.user.save()

    def test_login_exist(self):
        response = self.client.get(reverse('oauth:login'))
        self.assertEqual(response.status_code, 200)

    def test_login_using_login_func(self):
        found = resolve('/login/')
        self.assertEqual(found.func, login)

    def test_login_using_login_template(self):
        response = self.client.get(reverse('oauth:login'))
        self.assertTemplateUsed(response, 'login.html')

        self.assertContains(response, 'Username')
        self.assertContains(response, 'Password')

    def test_login_logged_in_is_redirect(self):
        self.client.login(username='jilham', password='test123')
        response = self.client.get(reverse('oauth:login'))
        self.assertEqual(response.status_code, 302)
    
    def test_login_not_logged_in(self):
        response = self.client.get(reverse('oauth:login'))
        html = response.content.decode()
        self.assertIn('<form', html)

    def test_login_submit(self):
        response = self.client.post(
            reverse('oauth:login'), data={
                'username': 'jilham',
                'password' : 'test123',
            }
        )
        
        self.assertEqual(response.status_code, 302)
        response = self.client.get(reverse('homepage:status'))
        html = response.content.decode()
        self.assertIn(self.user.username, html)
    
    def test_login_logout(self):
        response = self.client.post(
            reverse('oauth:login'), data={
                'username': 'jilly',
                'password' : 'password',
            }
        )

        self.client.get(reverse('oauth:logout'))
        response = self.client.get(reverse('homepage:status'))
        html = response.content.decode()
        self.assertNotIn(self.user.username, html)
