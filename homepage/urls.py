from django.urls import path
from . import views

app_name = "homepage"

urlpatterns = [
    path('', views.status, name='status'),
    path('about/', views.about, name='about'),
]
