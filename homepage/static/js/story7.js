///----- Auto-Close Accordion
$(document).ready(function() {
    //--- initial state of elements
     $('.slide').hide()
    //----- click function
    $("#accordion").find("div[role|='button']").click(function() { //---- tabs or buttons
        //---- active class
        $("#accordion").find("div[role|='button']").removeClass('active');
        $('.slide').slideUp('fast');
        var selected = $(this).next('.slide');
        if (selected.is(":hidden")) {
            $(this).next('.slide').slideDown('fast');
            $(this).toggleClass('active');
            }
    });
});

function changeStyleSummerBreeze() {
    document.body.style.backgroundColor = "#D2E0ED";
    document.getElementsByClassName("navbar")[0].style.backgroundColor = "#B8DADF";
    document.getElementsByTagName("span")[0].style.color = "#324C4C";
    document.getElementsByClassName("login-btn")[0].style.backgroundColor = "#324C4C";

    var navbar = document.getElementsByClassName("nav-link");
        for (var i = 0; i<navbar.length; i++) {
            navbar[i].style.color = "#324C4C";
        }

    if(document.title == "JL | About Me") {
        document.getElementById("about").style.backgroundColor = "#D2E0ED";
        document.getElementsByTagName("h1")[0].style.color = "#324C4C";
        document.getElementsByTagName("h2")[0].style.color = "#324C4C";
        document.getElementsByTagName("h2")[1].style.color = "#324C4C";

        var btn = document.getElementsByClassName("button");
        for (var i = 0; i<btn.length; i++) {
            btn[i].style.backgroundColor = "#FAC8C9";
            btn[i].style.borderBottomColor = "#EB8299";
            btn[i].style.color = "#324C4C";
            document.getElementsByClassName('title')[i].style.color = "#324C4C";
            document.getElementsByClassName('box')[i].style.borderColor = "#EB8299";
            document.getElementsByTagName("p")[i].style.color = "#324C4C";
            document.getElementsByTagName("p")[i].style.borderColor = "#EB8299";
        }
    }

    if(document.title == "JL | Live Status") {
        document.getElementById("status").style.backgroundColor = "#D2E0ED";   
        document.getElementsByClassName('text-center')[0].getElementsByTagName('h1')[0].style.color = "#324C4C";
        document.getElementById('status').getElementsByClassName('btn')[0].style.backgroundColor = "#EB8299";
        document.getElementById('status').getElementsByClassName('btn')[0].style.color = "#324C4C";
        var header = document.getElementsByTagName('thead')[0].getElementsByClassName('header');
        for(var i=0;i<header.length;i++){
            header[i].style.backgroundColor = "#FFEEEC";
            header[i].style.color = "#324C4C";
        }
        var content = document.getElementsByTagName('td');
        for(var i=0;i<content.length;i++){
            content[i].style.backgroundColor = "#D2E0ED"
            content[i].style.color = "#324C4C";
        }     
    }

    if (document.title == "JL | Search Book"){
        var bookSearch = document.getElementById("book");
        bookSearch.style.backgroundColor = "#D2E0ED";
        bookSearch.getElementsByTagName("label")[0].style.color = "#324C4C";
        bookSearch.getElementsByClassName('btn')[0].style.backgroundColor = "#EB8299";
        bookSearch.getElementsByClassName('btn')[0].style.color = "#324C4C";
        var header = document.getElementsByTagName('thead')[0].getElementsByClassName('header');
        for(var i=0;i<header.length;i++){
            header[i].style.backgroundColor = "#FFEEEC";
            header[i].style.color = "#324C4C";
        }
        var content = document.getElementsByTagName('td');
        for(var i=0;i<content.length;i++){
            content[i].style.backgroundColor = "#D2E0ED"
            content[i].style.color = "#324C4C";
        }
    }

    if (document.title == "JL | Login") {
        document.getElementById("login").style.backgroundColor = "#D2E0ED";
        document.getElementsByTagName('label')[0].style.color = "#324C4C";
        document.getElementsByTagName('label')[1].style.color = "#324C4C";
        document.getElementById('login').getElementsByClassName('btn')[0].style.backgroundColor = "#EB8299";
        document.getElementById('login').getElementsByClassName('btn')[0].style.color = "#324C4C";
    }
    
}

function changeStyleMidnightSky() {
    document.body.style.backgroundColor = "#2B2F77"
    document.getElementsByClassName("navbar")[0].style.backgroundColor = "#070B34";
    document.getElementsByClassName("header-title")[0].style.color = "white";
    document.getElementsByTagName("span")[0].style.color = "white";
    document.getElementsByClassName("login-btn")[0].style.backgroundColor = "#483475";

    var navbar = document.getElementsByClassName("nav-link");
        for (var i = 0; i<navbar.length; i++) {
            navbar[i].style.color = "white";
        }

    if(document.title == "JL | About Me") {
        document.getElementById("about").style.backgroundColor = "#2B2F77";
        document.getElementsByTagName("h1")[0].style.color = "white";
        document.getElementsByTagName("h2")[0].style.color = "white";
        document.getElementsByTagName("h2")[1].style.color = "white";

        var btn = document.getElementsByClassName("button");
        
        for (var i = 0; i<btn.length; i++) {
            btn[i].style.backgroundColor = "#070B34";
            btn[i].style.borderBottomColor = "#141852";
            btn[i].style.color = "white";
            document.getElementsByClassName('title')[i].style.color = "white";
            document.getElementsByClassName('box')[i].style.borderColor = "#141852";
            document.getElementsByTagName("p")[i].style.color = "white";
            document.getElementsByTagName("p")[i].style.borderColor = "#141852";
        }
    }

    if (document.title == "JL | Live Status") {
        document.getElementById("status").style.backgroundColor = "#2B2F77";
        document.getElementsByClassName('text-center')[0].getElementsByTagName('h1')[0].style.color = "white";
        document.getElementById('status').getElementsByClassName('btn')[0].style.backgroundColor = "#141852";
        document.getElementById('status').getElementsByClassName('btn')[0].style.color = "white";
        var header = document.getElementsByTagName('thead')[0].getElementsByClassName('header');
        for(var i=0;i<header.length;i++){
            header[i].style.backgroundColor = "#483475";
            header[i].style.color = "white";
        }
        var content = document.getElementsByTagName('td');
        for(var i=0;i<content.length;i++){
            content[i].style.backgroundColor = "#483475"
            content[i].style.color = "white";
        }
    }

    if (document.title == "JL | Search Book"){
        var bookSearch = document.getElementById("book");
        bookSearch.style.backgroundColor = "#2B2F77";
        bookSearch.getElementsByTagName("label")[0].style.color = "white";
        bookSearch.getElementsByClassName('btn')[0].style.backgroundColor = "#141852";
        bookSearch.getElementsByClassName('btn')[0].style.color = "white";
        var header = document.getElementsByTagName('thead')[0].getElementsByClassName('header');
        for(var i=0;i<header.length;i++){
            header[i].style.backgroundColor = "#483475";
            header[i].style.color = "white";
        }
        var content = document.getElementsByTagName('td');
        for(var i=0;i<content.length;i++){
            content[i].style.backgroundColor = "#483475"
            content[i].style.color = "white";
        }
    }

    if (document.title == "JL | Login") {
        document.getElementById("login").style.backgroundColor = "#2B2F77";
        document.getElementsByTagName('label')[0].style.color = "white";
        document.getElementsByTagName('label')[1].style.color = "white";
        document.getElementById('login').getElementsByClassName('btn')[0].style.backgroundColor = "#141852";
        document.getElementById('login').getElementsByClassName('btn')[0].style.color = "white";
    }

}