function addDataToTable(tbody, i, item) {
    var info = item.volumeInfo;
    var publisher = $('<p>').text("Publisher: ");
    publisher.append($('<nobr>').text(info.publisher ? info.publisher: '-'));
    var authors = info.authors ? info.authors : '-';
    var cover = info.imageLinks ? $('<img>').attr(
        {'src':  info.imageLinks.smallThumbnail.replace("http://", "https://")}
    ) : 'Info';
    var link = $('<a>').attr({'href': info.previewLink}).append(cover);
    var th_no = $('<td class="col-1 text-center">').attr({'scope': 'row'}).text(i + 1);
    var authors1 = $('<p>').text("Authors: "); 
    if (authors != '-') {
        $.each(authors, function(i, item) {
            authors1.append($('<nobr>').text(item));
        });
    } else {
        authors1.append($('<nobr>').text(authors));
    }
    var pages = $('<p>').text("Pages: ");
    pages.append($('<nobr>').text(info.pageCount ? info.pageCount : '-'));
    var date = $('<p>').text("Date: ");
    date.append($('<nobr>').text(info.publishedDate));
    var td_cover = $('<td class="col-3 text-center">').append(link);
    var title = $('<p>').text("Title: ");
    title.append($('<nobr>').text(info.title));
    var subTitle = $('<p>').text("Subtitle: ");
    subTitle.append($('<nobr>').text(info.subtitle));
    var td_information= $('<td class="col-8 text-wrap">').append(title,subTitle,authors1,publisher,date,pages);
    var $tr = $('<tr class="d-flex">').append(
        th_no,
        td_information,
        td_cover,
    ).appendTo(tbody);
}

function writeResults(items) {
    var val= document.getElementById("id_q").value;
    $('.result-text').show();
    $('#results-table').show();
    var tbody = $('#results');
    tbody.empty();
    $(function() {
        $.each(items, function(i, item) {
            addDataToTable(tbody, i, item);
        });
    });
    $('#results').show();
}


$(document).ready(function() {
    $('form').submit(function(event){
        // get form data
        var data = {
            'q': $('input[name=q]').val(),
            'maxResults': 10,
        };

        // process the form
        $.ajax({
            type        : 'GET',
            url         : 'volumes?' + $.param(data),
            dataType    : 'json',
        })
            .done(function(data){
                writeResults(data.items);            
            });
        event.preventDefault();
    })
});
