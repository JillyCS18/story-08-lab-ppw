from django.shortcuts import render
from django.http import JsonResponse
import requests

# Create your views here.

def books(request):
    return render(request, 'books.html')

def volumes(request):
    url = 'https://www.googleapis.com/books/v1/volumes'
    url += f"?maxResults={10}&q={request.GET.get('q')}"
    data = requests.get(url).json()
    return JsonResponse(data, json_dumps_params={'indent': 2})
